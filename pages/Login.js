import React, { Component } from 'react'
import DeviceInfo from 'react-native-device-info'
import styles from '../styles'
import * as UserAction from '../remx/User/actions'
import {encode} from 'base-64';
import md5 from 'md5'
import Spinner from 'react-native-loading-spinner-overlay'
import AsyncStorage from '@react-native-community/async-storage'
import Toast from 'react-native-simple-toast';
import packageJson from '../package.json';
import {
	StyleSheet,
	View,
	StatusBar,
	Alert,
	ImageBackground,
	Image,
	Keyboard,
	Dimensions,
	BackHandler,
} from 'react-native'

import {
	Button,
	Text,
	Item,
	Input,
	Card,
	Icon,
} from 'native-base'
import { REST_URL, handleBack, HEADERS_CONFIG } from 'larisa/AppConfig';

export default class Login extends Component {
	constructor(props) {
		super(props)
		this.state = {
			username: '',
			password: '',
			showLoading: false,
		}
	}

	componentDidMount() {
		// AsyncStorage.setItem('nip', 'P81190');
		this.backHandler = BackHandler.addEventListener(
			'hardwareBackPress',
			handleBack,
		);

		this.props.navigation.addListener('willBlur', () =>
			this.backHandler.remove(),
		);
	}

	render() {

		return (
			<ImageBackground
				source={require('assets/bg-login.png')}
				style={{ width: Dimensions.get('screen').width, height: Dimensions.get('screen').height }} >

				<View style={style.container}>
					<StatusBar
						backgroundColor={styles.statusbarAccent.backgroundColor}
						barStyle="light-content" />

					<Spinner
						visible={this.state.showLoading}
						color={styles.statusbarAccent.backgroundColor}
						size='large' />

					<Image
						source={require('assets/logo.png')}
						style={style.logo} />

					<Card transparent style={style.loginBox}>

						<Item
							regular
							style={style.textInput}>
							<Icon active name='user' type="AntDesign" />
							<Input
								placeholder="Username"
								onChangeText={(text) => this.setState({ username: text })} />
						</Item>

						<Item
							regular
							style={style.textInput}>
							<Icon active name='lock' type="AntDesign" />
							<Input
								placeholder="Password"
								secureTextEntry={true}
								onChangeText={(password) => this.setState({ password })} />
						</Item>

						<View style={{ display: 'flex', width: '90%' }}>
							{/* <LinearGradient colors={['rgba(242,209,101,1)', 'rgba(196,141,39,1)']}> */}

							<Button
								success
								block
								style={styles.buttonPrimay/* {backgroundColor: '#0966B9'} */}
								onPress={() => this.doLogin()}>
								<Text>Login</Text>
							</Button>

							{/* </LinearGradient> */}
						</View>

					</Card>

					<Text style={{ textAlign: 'center', top: '10%', fontSize: 16, fontWeight: '100', color: '#fff' }}>
						v.{packageJson.version}
					</Text>

				</View>
			</ImageBackground>
		);
	}

	getToken = () =>
		new Promise((resolve, reject) => {
			const enc = encode(encode(this.state.username));
			var a = enc.substr(0, (enc.length - (enc.length % 2)) / 2);
			var b = enc.substr(
				(enc.length - (enc.length % 2)) / 2,
				enc.length - (enc.length - (enc.length % 2)) / 2,
			);
			var c = (b.match(new RegExp('=', 'g')) || []).length;
			b = b.replace(new RegExp('=', 'g'), '');

			const tokenData = new FormData();
			tokenData.append('ap', encode(a));
			tokenData.append('bp', encode(b));
			tokenData.append('cp', encode(c));

			fetch(`${REST_URL}/tokken.php`, {
				method: 'POST',
				headers: {
					'Content-Type': 'multipart/form-data',
					...HEADERS_CONFIG.headers,
				},
				body: tokenData,
			})
				.then(response => response.json())
				.then(res => {
					if (res.status == '1') {
						resolve(res);
					} else {
						reject(`Forbidden`);
					}
				})
				.catch(err => {
					reject(`Error : ${err}`);
				});
		});

	doLogin() {
		// do nothing when username and password is empty
		if (this.state.username == '' && this.state.password == '') return false;

		const LOGIN_URL = `${REST_URL}/login.php`;
		Keyboard.dismiss();

		this.setState({ showLoading: true });
		// get token for login submission
		this.getToken()
			.then(response => {
				let { token, status } = response;
				const formData = new FormData();
				formData.append('nip', this.state.username);
				formData.append('password', md5(this.state.password));
				formData.append('masuk', status);
				formData.append('token', token);
				formData.append('imei', DeviceInfo.getUniqueId());

				// do login process
				fetch(LOGIN_URL, {
					method: 'POST',
					headers: {
						'Content-Type': 'multipart/form-data',
						...HEADERS_CONFIG.headers,
					},
					body: formData,
				})
					.then(response => response.json())
					.then(res => {
						this.setState({ showLoading: false });
						if (res.error) {
							this.setState({ password: '' });
							Toast.show(`Error : ${res.error_msg}`, Toast.LONG);
						} else if (res.success) {
							if (res.success_msg == null || res.success_msg == '') {
								let userNip = res.user.nip;
								AsyncStorage.setItem('nip', userNip).then(() => {
									// get user detail, store to remx
									UserAction.getUserDetail(userNip);
									// ServiceAction.getMyRequest(userNip);
									// ServiceAction.getTodoList(userNip);
									this.props.navigation.navigate('Home');
								});
							} else {
								Alert.alert('Info', `${res.success_msg}`, [{ text: 'OK' }]);
							}
						}
					})
					.catch(err => {
						this.setState({ showLoading: false });
						Toast.show(`Error when doing login :\n ${err}`, Toast.SHORT);
					});
			})
			.catch(err => {
				this.setState({ showLoading: false });
				Toast.show(`Error getting token: ${err}`, Toast.SHORT);
			});
	}
}

const style = StyleSheet.create({
	logo: {
		width: Dimensions.get('screen').width * 0.8,
		height: Dimensions.get('screen').height * 0.08,
		marginBottom: 20,
		resizeMode: 'stretch',
	},
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	textInput: {
		width: '90%',
		// borderWidth: 1,
		// borderColor: '#ccc',
		borderRadius: 5,
		paddingLeft: 10,
		marginBottom: 10,
		// color: '#fff',
		backgroundColor: '#fff',
	},
	loginBox: {
		width: '90%',
		alignContent: 'center',
		alignItems: 'center',
		backgroundColor: 'rgba(207,207,207,0.4)',
		paddingTop: 20,
		paddingBottom: 20,
		borderRadius: 10,
	}
})
