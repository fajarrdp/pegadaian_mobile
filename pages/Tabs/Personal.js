import React, { Component } from 'react';
import { connect } from 'remx';
import HeaderLayout from '../HeaderLayout';
import { store } from '../../remx/Service/store';
import * as UserAction from '../../remx/User/actions'
import { store as UserStore } from '../../remx/User/store';
import styles from '../../styles';
import AsyncStorage from '@react-native-community/async-storage';
import {
  Container,
  Content,
  Text,
  Card,
  ListItem,
  List,
  Item,
  Label,
  Icon,
  Left,
} from 'native-base';

import {
  Image,
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

class TabPersonal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    UserAction.checkSession()
  }

  clearLogin() {
    store.setTodoListData([]);
    store.setMyRequestData([]);

    AsyncStorage.clear().then(() => {
      this.props.navigation.reset({
        index: 0,
        routes: [{ name: 'Login' }],
      });
    });
  }

  logoutButton = () => {
    if (!this.props.route) {
      return (
        <TouchableOpacity
          style={style.btnLogout}
          onPress={() => {
            this.clearLogin();
          }}>
          <Icon name="log-out" type="Feather" style={{ left: 10 }} />
          <Text style={{ left: 15, fontWeight: 'bold' }}>Keluar</Text>
        </TouchableOpacity>
      );
    }
  };

  header = () => {
    if (this.props.route) {
      return (
        <HeaderLayout
          title="Info Pegawai"
          navigation={this.props.navigation}
        />
      );
    }
  };

  render() {
    let { user } = this.props;
    if (this.props.route) user = this.props.route.params.data;

    return (
      <Container>
        {this.header()}
        <Content bouncesZoom={true} showsVerticalScrollIndicator={false}>
          <ImageBackground
            source={require('assets/wall.png')}
            style={[styles.wallImageWrapper, { width: '100%', height: 200 }]}
          />

          <Card transparent style={{ alignItems: 'center', bottom: 50 }}>
            <Image
              source={
                user.picture
                  ? { uri: user.picture }
                  : require('assets/default.png')
              }
              style={[
                styles.displayPicture,
                { width: 100, height: 100, borderRadius: 50 },
              ]}
            />
            <Text
              style={[
                styles.userBoxName,
                styles.buttonPrimay,
                { textAlign: 'center' },
              ]}>
              {user.nm_peg ?? 'Loading...'}
            </Text>
            <Text
              note
              style={{
                color: '#000',
                top: 15,
                fontSize: 14,
                fontWeight: 'bold',
                textAlign: 'center',
              }}>
              {user.nip ?? 'Loading...'}
            </Text>
            {/* <Text note style={{color: '#000', top:15, fontSize:14, fontWeight: '100', textAlign:'center'}}>{user.jabatan} {(user.jns_kantor && user.jns_kantor.trim() == '1')?user.nm_unit_es3:user.nm_kantor} {user.nm_pangkat}</Text> */}
          </Card>
          <ListItem itemDivider />
          <List>
            <Item style={{ flexDirection: 'column', padding: 10 }}>
              <Card transparent style={{ width: '100%', flexDirection: 'row' }}>
                <Image
                  source={require('assets/user.png')}
                  style={styles.imageInputLabel}
                />
                <Label style={[styles.textInputLabel, { width: '100%' }]}>
                  Jabatan
                </Label>
              </Card>
              <Text style={{ width: '100%', left: 25 }}>{user.jabatan ?? '-'}</Text>
            </Item>
            <Item style={{ flexDirection: 'column', padding: 10 }}>
              <Card transparent style={{ width: '100%', flexDirection: 'row' }}>
                <Image
                  source={require('assets/user.png')}
                  style={styles.imageInputLabel}
                />
                <Label style={[styles.textInputLabel, { width: '100%' }]}>
                  Kantor
                </Label>
              </Card>
              <Text style={{ width: '100%', left: 25 }}>{user.nm_kantor ?? '-'}</Text>
            </Item>
            <Item style={{ flexDirection: 'column', padding: 10 }}>
              <Card transparent style={{ width: '100%', flexDirection: 'row' }}>
                <Image
                  source={require('assets/user.png')}
                  style={styles.imageInputLabel}
                />
                <Label style={[styles.textInputLabel, { width: '100%' }]}>
                  Pangkat
                </Label>
              </Card>
              <Text
                style={{
                  width: '100%',
                  left: 25,
                }} /* onPress={()=>Linking.openURL(`tel:${user.phone}`)} */
              >
                {user.nm_pangkat ?? '-'}
              </Text>
            </Item>
            <Item style={{ flexDirection: 'column', padding: 10 }}>
              <Card transparent style={{ width: '100%', flexDirection: 'row' }}>
                <Image
                  source={require('assets/user.png')}
                  style={styles.imageInputLabel}
                />
                <Label style={[styles.textInputLabel, { width: '100%' }]}>
                  Atasan Langsung
                </Label>
              </Card>
              <Text
                style={{
                  width: '100%',
                  left: 25,
                }}
              >
                {user.atasan ?? '-'}
              </Text>
            </Item>
          </List>
          <ListItem itemDivider />
          {this.logoutButton()}
          <ListItem itemDivider />
        </Content>
      </Container>
    );
  }
}

const style = StyleSheet.create({
  btnLogout: {
    flexDirection: 'row',
    height: 40,
    alignItems: 'center',
  },
});

function mapStateToProps() {
  return {
    user: UserStore.getUserData(),
  };
}

export default connect(mapStateToProps)(TabPersonal);
