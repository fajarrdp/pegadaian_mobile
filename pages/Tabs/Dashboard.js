// @flow

import React, {Component} from 'react';
import { TouchableHighlight, Linking, Alert } from 'react-native';
import styles from '../../styles';
import {connect} from 'remx';
import * as action from '../../remx/Service/actions';
import {store as UserStore} from '../../remx/User/store';
import * as UserAction from '../../remx/User/actions';
import {store as ServiceStore} from '../../remx/Service/store';
import Carousel, {PaginationLight} from 'react-native-x-carousel';
import AsyncStorage from '@react-native-community/async-storage';
import {
  Container,
  Content,
  CardItem,
  Body,
  Text,
  View,
  Badge,
  Toast,
  Footer,
} from 'native-base';

import {
  StyleSheet,
  ImageBackground,
  Image,
  TouchableOpacity,
  RefreshControl,
  Dimensions,
} from 'react-native';

const {width, height} = Dimensions.get('screen');

class TabDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };

    this.openLink.bind(this);
  }

  componentDidMount() {
    UserAction.checkSession();

    AsyncStorage.getItem('nip').then(result => {
      action.getMyRequest(result);
      action.getTodoList(result);
      this.setState({nip: result});
    });
    // action.getJenisTugas();
    action.getFeatureData();
    action.getImageBanner();

    console.log(this.props.imageBanner);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.user != UserStore.getUserData()) {
      this.setState({refreshing: false});
    }
  }

  doRefresh = () => {
    this.setState({refreshing: true});
    UserAction.getUserDetail(this.props.user.nip);

    /* Refresh MyRequest List */
    action.getMyRequest(this.props.user.nip);
    action.getTodoList(this.props.user.nip);
  };

  openLink(url) {
    if(url != null){
      var new_url = url.trim()
      if(new_url != ''){
        Linking.openURL(url).catch(err => console.error("Couldn't load page", err));
      } else {
        Alert.alert('Info', `Link tidak tersedia`, [
          { text: 'Close' },
        ]);
      }
    }
  }

  renderBanner = (image, index) => {
    return (
      <View key={index}>
        <TouchableHighlight onPress={() => this.openLink(image.go_to)}>
          <Image style={style.newsBanner} source={{uri: image.url}} />
        </TouchableHighlight>
      </View>
    );
  };

  featureCheck = (target: String) => {
    let {featureData} = this.props;
    if (featureData[target] == 'true') {
      switch (target) {
        case 'Leave':
          target = 'LeaveRequest';
          break;
        case 'Claim':
          target = 'ClaimRequest';
          break;
        case 'PaySlip':
          target = 'PaySlipList';
          break;
        default:
          break;
      }

      this.props.navigation.push(target);
    } else {
      Toast.show({
        text: `Fitur Belum dapat digunakan`,
        position: 'bottom',
        type: 'warning',
      });
    }
  };

  render() {
    let {user, todoList} = this.props;

    return (
      <Container>
        <Content style={{backgroundColor:'#fff'}}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.doRefresh}
              title="Loading..."
            />
          }
          showsVerticalScrollIndicator={false}
          bouncesZoom={true}>
          <ImageBackground
            source={require('assets/wall.png')}
            style={[{width: width}, styles.wallImageWrapper]}>
            <View style={{position: 'absolute', top: 10, left: 10}}>
              <Image
                source={
                  user.picture
                    ? {uri: user.picture}
                    : require('assets/default.png')
                }
                style={styles.displayPicture}
              />
              <View
                style={{
                  flexDirection: 'column',
                  position: 'absolute',
                  left: 80,
                }}>
                <Text style={styles.userBoxName}>
                  {user.nm_peg ?? 'Loading...'}
                </Text>
                <Text
                  note
                  style={{
                    color: '#fff',
                    top: 15,
                    fontSize: 14,
                    fontWeight: '100',
                  }}>
                  {user.nip ?? 'Loading...'}
                </Text>
              </View>
            </View>
          </ImageBackground>

          <View
            style={{
              display: 'flex',
              marginTop:height * 0.10,
              backgroundColor: '#F2D165',
              bottom: height * 0.25,
              padding: 10,
              width: width * 0.9,
              alignSelf: 'center',
              borderRadius: 10,
              alignItems: 'center',
            }}>
            {/* first row */}
            <View style={{display: 'flex', flexDirection: 'row'}}>
              <TouchableOpacity
                style={style.cardMenu}
                onPress={() => this.featureCheck('Leave')}>
                <CardItem style={{backgroundColor: 'transparent'}}>
                  <Body style={{alignItems: 'center'}}>
                    <Image
                      source={require('assets/menu/leave.png')}
                      style={style.iconMenu}
                    />
                    <Text style={style.menuText}>Cuti</Text>
                  </Body>
                </CardItem>
              </TouchableOpacity>
              <TouchableOpacity
                style={style.cardMenu}
                onPress={() => this.featureCheck('Claim')}>
                <CardItem style={{backgroundColor: 'transparent'}}>
                  <Body style={{alignItems: 'center'}}>
                    <Image
                      source={require('assets/menu/reimbursement.png')}
                      style={style.iconMenu}
                    />
                    <Text style={style.menuText}>Klaim</Text>
                  </Body>
                </CardItem>
              </TouchableOpacity>
              <TouchableOpacity
                style={style.cardMenu}
                onPress={() => this.featureCheck('Attendance')}>
                <CardItem style={{backgroundColor: 'transparent'}}>
                  <Body style={{alignItems: 'center'}}>
                    <Image
                      source={require('assets/menu/attendance.png')}
                      style={style.iconMenu}
                    />
                    <Text style={style.menuText}>Kehadiran</Text>
                  </Body>
                </CardItem>
              </TouchableOpacity>
            </View>
            {/* second row */}
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                style={style.cardMenu}
                onPress={() => this.featureCheck('Worklist')}>
                <CardItem style={{backgroundColor: 'transparent'}}>
                  <Body style={{alignItems: 'center'}}>
                    <Image
                      source={require('assets/menu/worklist.png')}
                      style={style.iconMenu}
                    />
                    <Text style={style.menuText}>Daftar Pekerjaan</Text>
                    {todoList.length > 0 && (
                      <View
                        style={{
                          position: 'absolute',
                          left: width * 0.125,
                        }}>
                        <Badge style={{bottom: 10}}>
                          <Text>{todoList.length}</Text>
                        </Badge>
                      </View>
                    )}
                  </Body>
                </CardItem>
              </TouchableOpacity>
              <TouchableOpacity
                style={style.cardMenu}
                onPress={() => this.featureCheck('PaySlip')}>
                <CardItem style={{backgroundColor: 'transparent'}}>
                  <Body style={{alignItems: 'center'}}>
                    <Image
                      source={require('assets/menu/payslip.png')}
                      style={style.iconMenu}
                    />
                    <Text style={style.menuText}>Slip Gaji</Text>
                  </Body>
                </CardItem>
              </TouchableOpacity>
              <TouchableOpacity
                style={style.cardMenu}
                onPress={() => this.featureCheck('SearchEmployee')}>
                <CardItem style={{backgroundColor: 'transparent'}}>
                  <Body style={{alignItems: 'center'}}>
                    <Image
                      source={require('assets/menu/search.png')}
                      style={style.iconMenu}
                    />
                    <Text style={style.menuText}>Cari Karyawan</Text>
                  </Body>
                </CardItem>
              </TouchableOpacity>
              {/* <TouchableOpacity style={style.cardMenu} onPress={()=> this.featureCheck('SuratTugas')}>
                <CardItem style={{backgroundColor: 'transparent'}}>
                  <Body style={{alignItems: 'center',}}>
                    <Image source={require("assets/menu/sppd.png")} style={style.iconMenu} />
                    <Text style={style.menuText}>Surat Tugas</Text>
                  </Body>
                </CardItem>
              </TouchableOpacity> */}
            </View>
            {/* Third Row */}
            {/* <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity style={style.cardMenu} onPress={()=> this.featureCheck('QRScanner')}>
                <CardItem style={{backgroundColor: 'transparent'}}>
                  <Body style={{alignItems: 'center',}}>
                    <Image source={require("assets/menu/qr-code.png")} style={style.iconMenu} />
                    <Text style={style.menuText}>Pindai QR</Text>
                  </Body>
                </CardItem>
              </TouchableOpacity>
              <TouchableOpacity style={style.cardMenu} onPress={() => this.featureCheck('Worklist')}>
                <CardItem style={{backgroundColor: 'transparent'}}>
                  <Body style={{alignItems: 'center'}}>
                    <Image source={require("assets/menu/worklist.png")} style={style.iconMenu} />
                    <Text style={style.menuText}>Daftar Pekerjaan</Text>
                    { todoList.length>0 &&
                      <View style={{position:'absolute', left: width*0.125}}>
                        <Badge style={{bottom: 10}}><Text>{todoList.length}</Text></Badge>
                      </View>
                    }
                  </Body>
                </CardItem>
              </TouchableOpacity>
              <TouchableOpacity style={style.cardMenu} onPress={() => this.featureCheck('EmployeeRequestIdx')}>
                <CardItem style={{backgroundColor: 'transparent'}}>
                  <Body style={{ alignItems: 'center', }}>
                    <Image source={require("assets/menu/sppd.png")} style={style.iconMenu} />
                    <Text style={style.menuText}>Pengajuan FPPK</Text>
                  </Body>
                </CardItem>
              </TouchableOpacity>
            </View> */}
          </View>

          {/* NEWS BANNER */}
          <View
            style={{
              position: 'absolute',
              bottom: -20,
              padding: 10,
              width: width * 0.9,
              alignSelf: 'center',
              flexDirection: 'column',
            }}>
            <Text style={{fontWeight: 'bold'}}>News</Text>

            <View
              style={{
                backgroundColor: '#fff',
                width: width * 0.9,
                height: height * 0.22,
                alignSelf: 'center',
                borderRadius: 10,
                alignItems: 'center',
              }}>
              {this.props.imageBanner.length > 0 && (
                <Carousel
                  pagination={PaginationLight}
                  renderItem={this.renderBanner}
                  data={this.props.imageBanner}
                  loop={true}
                  autoplay={true}
                  autoplayInterval={40000}
                />
              )}
            </View>
          </View>
        </Content>
        <Footer style={{display:'none', backgroundColor: '#282829'}}>
          <Image
            source={require('assets/footer-quotes.png')}
            style={{
              top: 10,
              left: 0,
              right: 0,
              position: 'absolute',
              width: width,
              height: height * 0.05,
              resizeMode: 'cover',
            }}
          />
        </Footer>
      </Container>
    );
  }
}

const style = StyleSheet.create({
  cardMenu: {
    width: width * 0.3,
    // borderColor: '#F2D165',
    // backgroundColor: 'rgba(52, 52, 52, 0.8)',
    // borderBottomWidth: 1,
    // borderLeftWidth: 1,
    // borderRightWidth: 1,
    // borderRadius: 0,
    minHeight: 90,
    maxHeight: 130,
    marginRight: 0,
    marginLeft: 0,
    marginTop: 0,
    textAlign: 'center',
  },
  disabledCardMenu: {
    width: width * 0.25,
    backgroundColor: '#ddd',
    borderColor: '#ccc',
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderRadius: 0,
    minHeight: 90,
    maxHeight: 130,
    marginRight: 0,
    marginLeft: 0,
    marginTop: 0,
    textAlign: 'center',
  },
  iconMenu: {
    width: 80,
    height: 80,
    resizeMode: 'stretch',
    bottom: 5,
  },
  menuText: {
    fontSize: width * 0.03,
    top: 2,
    textAlign: 'center',
    fontWeight: '500',
  },
  newsBanner: {
    height: height * 0.20,
    width: width * 0.9,
    resizeMode: 'stretch',
  },
});

function mapStateToProps() {
  return {
    user: UserStore.getUserData(),
    todoList: ServiceStore.getTodoListData(),
    featureData: ServiceStore.getFeatureData(),
    imageBanner: ServiceStore.getImageBanner(),
  };
}

export default connect(mapStateToProps)(TabDashboard);
