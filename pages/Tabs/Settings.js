import React, { Component } from 'react';
import { connect } from 'remx';
import { store as ServiceStore } from '../../remx/Service/store';
import { store as UserStore } from '../../remx/User/store';
import * as UserAction from '../../remx/User/actions';
import {
  Container,
  Content,
  Body,
  Text,
  Left,
  ListItem,
  Icon,
  Button,
  Right,
} from 'native-base';

import { TouchableOpacity, Alert } from 'react-native';

class TabSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    UserAction.checkSession();
  }

  featureCheck = target => {
    const { featureData } = this.props;
    if (featureData[target] == 'true') {
      this.props.navigation.push(target);
    } else {
      Alert.alert('Info', `Fitur belum dapat digunakan`, [{ text: 'Close' }]);
    }
  };

  render() {
    return (
      <Container>
        <Content>
          {/* <ListItem icon style={{backgroundColor: '#ddd', marginLeft: 0, paddingLeft: 18}}>
            <TouchableOpacity style={{flexDirection: 'row'}}>
              <Left>
                <Button success>
                  <Icon active name='ios-person' />
                </Button>
              </Left>
              <Body>
                <Text>Ubah Profil</Text>
              </Body>
              <Right>
                <Icon active name='ios-arrow-forward' />
              </Right>
            </TouchableOpacity>
          </ListItem> */}
          <ListItem icon style={{ marginLeft: 0, paddingLeft: 18 }}>
            <TouchableOpacity
              style={{ flexDirection: 'row' }}
              onPress={() => this.featureCheck('ChangePassword')}>
              <Left>
                <Button success>
                  <Icon active name="ios-unlock" />
                </Button>
              </Left>
              <Body>
                <Text>Ubah Katasandi</Text>
              </Body>
              <Right>
                <Icon active name="ios-arrow-forward" />
              </Right>
            </TouchableOpacity>
          </ListItem>
          {/* <ListItem icon style={{backgroundColor: '#ddd', marginLeft: 0, paddingLeft: 18}}>
            <TouchableOpacity style={{flexDirection: 'row'}}>
              <Left>
                <Button success>
                  <Icon active name='ios-refresh' />
                </Button>
              </Left>
              <Body>
                <Text>Atur Ulang Aplikasi</Text>
              </Body>
              <Right>
                <Icon active name='ios-arrow-forward' />
              </Right>
            </TouchableOpacity>
          </ListItem> */}
          <ListItem icon style={{ marginLeft: 0, paddingLeft: 18 }}>
            <TouchableOpacity
              style={{ flexDirection: 'row' }}
              onPress={() => this.featureCheck('RateApp')}>
              <Left>
                <Button style={{ backgroundColor: '#007AFF' }}>
                  <Icon active name="ios-star-outline" />
                </Button>
              </Left>
              <Body>
                <Text>Beri Nilai LARISA</Text>
              </Body>
              <Right>
                <Icon active name="ios-arrow-forward" />
              </Right>
            </TouchableOpacity>
          </ListItem>
        </Content>
      </Container>
    );
  }
}

function mapStateToProps() {
  return {
    featureData: ServiceStore.getFeatureData(),
    user: UserStore.getUserData(),
  };
}

export default connect(mapStateToProps)(TabSettings);
