import React, { Component } from 'react';
import HeaderLayout from '../HeaderLayout';
import { connect } from 'remx';
import { store } from '../../remx/Absensi/store';
import * as actions from '../../remx/Absensi/actions';
import styles from '../../styles';
import { MONTH_ITEMS_OPTION } from "larisa/AppConfig";
import {
  StyleSheet,
  ActivityIndicator,
  Dimensions,
  FlatList,
  View,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import {
  Container,
  Item,
  Picker,
  Button,
  Text,
  ListItem,
  Grid,
  Row,
  Col,
  Label,
} from 'native-base';

class AttendaceList extends Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = {
      nip: '',
      selectedYear: new Date().getFullYear(),
      selectedMonth: new Date().getMonth() + 1,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('nip').then(result => {
      this.setState({ nip: result });

      let { month, year } = this.props;
      if (month && year) {
        this.setState({ selectedMonth: month, selectedYear: year });
      }

      if ('' == store.getData().toString()) {
        actions.setMonth(this.state.selectedMonth);
        actions.setYear(this.state.selectedYear);
        actions.getAbsensiData(result);
      }
    });
  }

  submitButton = () => {
    store.setData([]);
    store.setLoading(true);
    actions.setMonth(this.state.selectedMonth);
    actions.setYear(this.state.selectedYear);
    actions.getAbsensiData(this.state.nip);
  };

  generateList(items) {
    const v = items.item;
    return (
      <Row
        style={{
          backgroundColor:
            v.color != ''
              ? '#' + v.color
              : items.index % 2 == 0
                ? '#DBEDFC'
                : '#fff',
          paddingTop: 10,
          paddingBottom: 10,
        }}>
        <Col size={33} style={style.centerCol}>
          <Text>{v.tanggal_absen}</Text>
        </Col>
        <Col size={33} style={style.centerCol}>
          <Text>{v.jam_masuk}</Text>
        </Col>
        <Col size={33} style={style.centerCol}>
          <Text>{v.jam_keluar}</Text>
        </Col>
      </Row>
    );
  }

  yearOptionItems = () => {
    var arr = [];
    let a = new Date().getFullYear();
    for (let i = a; i > a - 5; i--) {
      arr.push(`${i}`);
    }
    return arr;
  }

  render() {
    const { data } = this.props;

    return (
      <Container>
        <HeaderLayout
          title="Daftar Kehadiran"
          navigation={this.props.navigation}
        />
        <View>
          <Item last>
            <Picker
              note
              mode="dropdown"
              placeholder="Month"
              selectedValue={this.state.selectedMonth}
              onValueChange={val => this.setState({ selectedMonth: val })}
              style={{ width: Dimensions.get('screen').width * (40 / 100) }}>
              {MONTH_ITEMS_OPTION.map((label, i) => (
                <Picker.Item label={label} value={i + 1} key={i} />
              ))}
            </Picker>

            <Picker
              note
              mode="dropdown"
              placeholder="Year"
              selectedValue={this.state.selectedYear}
              onValueChange={val => this.setState({ selectedYear: val })}
              style={{ width: Dimensions.get('screen').width * (40 / 100) }}>
              {this.yearOptionItems().map((label, i) => (
                <Picker.Item label={label} value={label} key={i} />
              ))}
            </Picker>

            <Button
              style={[
                styles.buttonPrimay,
                {
                  height: '100%',
                  borderRadius: 0,
                  width: Dimensions.get('screen').width * (20 / 100),
                },
              ]}
              onPress={this.submitButton}>
              <Text>Lihat</Text>
            </Button>
          </Item>
          <ListItem itemDivider />

          <Item style={{ height: 50 }}>
            <Col size={33} style={style.centerCol}>
              <Label style={style.headerLabel}>Tanggal</Label>
            </Col>
            <Col size={33} style={style.centerCol}>
              <Label style={style.headerLabel}>Jam Masuk</Label>
            </Col>
            <Col size={33} style={style.centerCol}>
              <Label style={style.headerLabel}>Jam Keluar</Label>
            </Col>
          </Item>
          {this.props.isLoading && (
            <ActivityIndicator
              size="large"
              style={{ height: (60 / 100) * Dimensions.get('screen').height }}
              color={styles.loading.color}
            />
          )}
          <FlatList
            data={data}
            renderItem={item => this.generateList(item)}
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
            initialNumToRender={6}
          />
        </View>
      </Container>
    );
  }
}

const style = StyleSheet.create({
  centerCol: {
    alignItems: 'center',
  },
  headerLabel: {
    fontWeight: 'bold',
    color: '#000',
  },
});

function mapStateToProps(ownProps) {
  return {
    data: store.getData(),
    isLoading: store.isLoading(),
    year: store.getYear(),
    month: store.getMonth(),
  };
}

export default connect(mapStateToProps)(AttendaceList);
