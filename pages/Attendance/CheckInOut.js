import { connect } from 'remx';
import { store as UserStore } from '../../remx/User/store';
import React, { Component } from 'react';
import * as RNLocalize from 'react-native-localize';
import { RNCamera } from 'react-native-camera';
import Spinner from 'react-native-loading-spinner-overlay';
// import Geolocation from '@react-native-community/geolocation';
import Geolocation from "react-native-geolocation-service";
import packageJson from '../../package.json';
import { REST_URL, HEADERS_CONFIG } from 'larisa/AppConfig';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Platform,
  Dimensions,
  Alert,
  PermissionsAndroid,
} from 'react-native';

import { Icon, Text } from 'native-base';
import Toast from 'react-native-simple-toast';

class CheckInOut extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showLoading: false,
      lat: '',
      long: '',
      location: null,
      isCameraReady: false,
    };
  }

  componentDidMount() {
    this.getLocationName();
  }

  getLatLongPosition = new Promise((resolve, reject) => {
    // request permission
    if (Platform.OS == 'android') {
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      ).then(re => {
        if (re != PermissionsAndroid.RESULTS.GRANTED)
          this.props.navigation.navigate('Attendance',this.props)
      });
    } else {
      Geolocation.requestAuthorization()
    }

    // this.setState({location: 'Sedang mendapatkan lokasi...'});
    Geolocation.getCurrentPosition(
      result => {
        let { coords, mocked } = result;
        if (Platform.OS == 'android' && mocked) {
          this.props.navigation.navigate('Attendance',this.props)
          reject({
            alert: `Harap matikan penetapan lokasi palsu Anda untuk melanjutkan!`,
          });
        }
        this.setState({ lat: coords.latitude, long: coords.longitude });
        resolve();
      },
      err => {
        this.setState({ location: `Gagal Mendapatkan Lokasi` });
        reject({ toast: err });
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 },
    );
  });

  getLocationName = () => {
    this.getLatLongPosition
      .then(() => {
        // get ws location referrer
        fetch(`${REST_URL}/location_refferer.php`, HEADERS_CONFIG)
          .then(locUri => locUri.json())
          .then(resURL => {
            resURL.response = resURL.response
              .replace('${nip}', this.props.user.nip)
              .replace('${lat}', this.state.lat)
              .replace('${lon}', this.state.long);

            console.log(resURL.response);
            console.log(this.props.user.nip);

            // get geocoding name
            fetch(resURL.response, HEADERS_CONFIG)
              .then(response => response.json())
              .then(res => {
                let located = res.display_name;
                this.setState({ location: `${located.substring(0, 70)}...` });
              })
              .catch(err => {
                Toast.show(`Error: ${err}`);
                this.setState({ location: `Gagal Mendapatkan Lokasi` });
              });
          })
          .catch(err => {
            Toast.show(`Error: ${err}`);
            this.setState({ location: `Gagal Mendapatkan Lokasi` });
          });
      })
      .catch(err => {
        if (err.alert) {
          Alert.alert('Info', err.alert);
        } else {
          Toast.show(`Error: ${err.toast.message}`);
        }
      });
  };

  takePicture = async () => {
    if (this.camera) {
      this.setState({ showLoading: true });
      const options = {
        quality: 0.1,
        // base64: true,
        pauseAfterCapture: true,
        orientation: 'portrait',
        fixOrientation: true,
        width: 640,
        height: 640,
      };
      let data = await this.camera.takePictureAsync(options);
      this.doRecognize(data);
    }
  };

  doRecognize = async photo => {
    let uri =
      Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', '');
    let name = uri.split('/');
    name = name[name.length - 1];
    let contentType = 'image/jpeg';

    let formData = new FormData();
    formData.append('user_id', this.props.user.nip);
    formData.append('lat', this.state.lat);
    formData.append('lon', this.state.long);
    formData.append('location', this.state.location);
    formData.append('version', packageJson.version);
    formData.append('timezone', RNLocalize.getTimeZone());

    formData.append('file', {
      name: name,
      type: contentType,
      uri: uri,
    });

    let url = `${REST_URL}/absensi/post.php`;
    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        ...HEADERS_CONFIG.headers,
      },
      body: formData,
    })
      .then(response => response.json())
      .then(res => {
        this.setState({ showLoading: false });
        setTimeout(()=> {
          Alert.alert('Info', res.data);
        }, 100)
        
        if (!res.succes) {
          if (new RegExp('gagal', 'i').test(res.data)) {
            this.camera.resumePreview();
          } else {
            this.props.navigation.navigate('Attendance',this.props)
          }
        }
      })
      .catch(err => {
        this.setState({ showLoading: false });
        this.camera.resumePreview();
        Toast.show(`Error: ${err}`);
      });
  };

  render() {
    return (
      <View style={style.container}>
        <Spinner
          visible={this.state.showLoading}
          overlayColor="rgba(0, 0, 0, 0.25)"
          size="large"
        />

        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          type="front"
          style={style.preview}
          captureAudio={false}
          flashMode="off"
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        />

        <TouchableOpacity
          style={[style.mapLocationBox, { width: '90%' }]}
          onPress={() => {
            this.getLocationName();
          }}>
          <Icon name="map-marker" type="FontAwesome" />
          <View
            style={{
              flexDirection: 'column',
              position: 'absolute',
              left: 45,
              top: '25%',
              width: '100%',
            }}>
            <Text
              style={{
                fontWeight: '100',
                fontSize: Dimensions.get('window').width * 0.04,
                width: '90%',
                top: new RegExp('sedang|gagal mendapatkan', 'i').exec(
                  this.state.location,
                )
                  ? 10
                  : 0,
              }}>
              {this.state.location ?? 'Sedang mendapatkan lokasi...'}
            </Text>
          </View>
        </TouchableOpacity>

        {/* <View style={styles.captureWrapper}>

        </View> */}

        <View
          style={{
            position: 'absolute',
            bottom: 50,
            left: Dimensions.get('screen').width * 0.45 - 20,
            right: 0,
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={this.takePicture.bind(this)}
            style={style.capture}
            disabled={
              new RegExp('gagal mendapatkan', 'i').exec(this.state.location) ||
                this.state.location == null
                ? true
                : false
            }>
            <Icon name="camera" type="MaterialIcons" />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Attendance',this.props)}
            style={style.cancelButton}>
            <Icon name="x" type="Feather" style={{ color: 'white' }} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    // alignItems: 'center',
    // justifyContent: 'flex-end',
  },
  cancelButton: {
    width: 50,
    height: 50,
    padding: 8,
    borderRadius: 25,
    backgroundColor: 'red',
    left: 20,
    top: 10,
    borderColor: 'white',
    borderWidth: 2,
  },
  capture: {
    // flex: 1,
    backgroundColor: '#fff',
    borderRadius: 100,
    padding: Dimensions.get('screen').width * 0.05,
    // paddingHorizontal: Dimensions.get('screen').width*(8/100),
    alignSelf: 'center',
  },
  captureWrapper: {
    flex: 0,
    alignSelf: 'center',
    position: 'absolute',
    marginTop: Dimensions.get('window').height * 0.85,
  },
  mapLocationBox: {
    position: 'absolute',
    marginTop: Dimensions.get('screen').height * 0.05,
    backgroundColor: '#fff',
    padding: 15,
    alignSelf: 'center',
    borderRadius: 50,
    flexDirection: 'column',
  },
});

function mapStateToProps() {
  return {
    user: UserStore.getUserData(),
  };
}

export default connect(mapStateToProps)(CheckInOut);
