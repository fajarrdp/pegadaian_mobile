import { connect } from 'remx';
import React, { Component } from 'react';
import HeaderLayout from '../HeaderLayout';
import { store as UserStore } from '../../remx/User/store';
import * as UserAction from '../../remx/User/actions';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import CardListItem from '../CardListItem';
import style from '../../styles';
import { Container, Card, CardItem, Left, Right, Icon, Toast } from 'native-base';
import {
  View,
  Alert,
  FlatList,
  TouchableOpacity,
  Image,
  Text,
} from 'react-native';
import { REST_URL, HEADERS_CONFIG } from 'larisa/AppConfig';

const items = [
  {
    image: require('assets/menu/attendance2.png'),
    title: 'Daftar Kehadiran',
    route: 'AttendanceList',
  },
  {
    image: require('assets/menu/check-in.png'),
    title: 'Presensi',
    route: 'CheckInOut',
  },
  /* {
    image: require('assets/menu/overtime.png'),
    title: 'Permohonan Lembur',
    route: 'Overtime',
  }, */
  {
    image: require('assets/menu/attendance2.png'),
    title: 'Dispensasi Absensi',
    route: 'Dispensation',
  },
];

class Attendace extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nip: '',
      showLoading: false,
    };
  }

  componentDidMount() {
    UserAction.checkSession()
  }

  clearFaces = () => {
    this.setState({ showLoading: true });
    AsyncStorage.getItem('totalTraining', (err, res) => {
      if (res == '1') {
        this.setState({ showLoading: false });
        setTimeout(() => {
          Alert.alert('Info', `Data wajah sudah terhapus!`);
        }, 100);
        return false;
      } else {
        fetch(`${REST_URL}/absensi/clear_faces.php?nip=${this.props.user.nip}`, HEADERS_CONFIG)
          .then(response => response.json())
          .then(res => {
            this.setState({ showLoading: false });
            if (res.success) {
              AsyncStorage.setItem('totalTraining', '1').then(() => {
                Alert.alert('Info', res.message);
              });
            } else {
              Alert.alert('Info', res.message);
            }
          })
          .catch(err => {
            this.setState({ showLoading: false });
            Toast.show({
              text: `Error: ${err}`,
              type: 'danger'
            })
          });
      }
    });
  }

  registerFace = () => {
    AsyncStorage.getItem('totalTraining').then(result => {
      if (result > 3) {
        Alert.alert('Info', 'Anda sudah pernah mendaftarkan wajah', [
          { text: 'OK' },
        ]);
      } else {
        this.props.navigation.push('FaceRegister');
      }
    });
  }

  render() {

    return (
      <Container>
        <HeaderLayout navigation={this.props.navigation} title="Kehadiran" />
        <View>
          <Spinner
            visible={this.state.showLoading}
            overlayColor="rgba(0, 0, 0, 0.25)"
            size="large"
          />
          <FlatList
            data={items}
            renderItem={item => <CardListItem item={item} navigation={this.props.navigation} />}
            keyExtractor={(item, index) => `q${index}`}
            showsVerticalScrollIndicator={false}
          />
          <Card style={style.cardListItem}>
            <CardItem>
              <TouchableOpacity
                style={{ flexDirection: 'row' }}
                onPress={() => this.registerFace()}>
                <Image
                  source={require('assets/menu/facerecog.png')}
                  style={style.cardListImageItem}
                />
                <Left>
                  <Text style={style.cardTextTitle}>Pendaftaran wajah</Text>
                </Left>
                <Right>
                  <Icon active name="ios-arrow-forward" />
                </Right>
              </TouchableOpacity>
            </CardItem>
          </Card>
          <Card style={style.cardListItem}>
            <CardItem>
              <TouchableOpacity
                style={{ flexDirection: 'row' }}
                onPress={() => this.clearFaces()}>
                <Image
                  source={require('assets/menu/clear-face.png')}
                  style={style.cardListImageItem}
                />
                <Left>
                  <Text style={style.cardTextTitle}>Hapus data wajah</Text>
                </Left>
                <Right>
                  <Icon active name="ios-arrow-forward" />
                </Right>
              </TouchableOpacity>
            </CardItem>
          </Card>
        </View>
      </Container>
    );
  }
}

function mapStateToProps() {
  return {
    user: UserStore.getUserData(),
  };
}

export default connect(mapStateToProps)(Attendace);
