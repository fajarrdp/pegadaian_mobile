import React, {Component} from 'react';
import {store as UserStore} from '../remx/User/store';
import {connect} from 'remx';
import HeaderLayout from '../pages/HeaderLayout';
import {REST_URL, HEADERS_CONFIG} from 'larisa/AppConfig';
import styles from "../styles";
import {
  Container,
  Content,
  Text,
  Card,
  CardItem,
  Body,
  Item,
  Label,
  Form,
  Input,
  Footer,
  FooterTab,
  Button,
} from 'native-base';
import {Alert} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pass_old: '',
      pass_new: '',
      verify: '',
      isLoading: true,
      showLoading: false,
    };
  }

  submitForm = () => {
    let uri = `${REST_URL}/password.php?nip=${this.props.user.nip}&old=${this.state.pass_old}&password=${this.state.pass_new}&verify=${this.state.verify}`;

    this.setState({showLoading: true});

    fetch(uri, HEADERS_CONFIG)
      .then(response => response.json())
      .then(res => {
        this.setState({showLoading: false});
        if (res.error) {
          Alert.alert('Error!', `${res.error_msg}`, [{text: 'OK'}]);
        } else if (res.success) {
          if (res.success_msg == null || res.success_msg == '') {
            this.props.navigation.navigate('Home');
          } else {
            Alert.alert('Info', `${res.success_msg}`, [
              {
                text: 'OK',
                onPress: () => this.props.navigation.navigate('Home'),
              },
            ]);
          }
        }
      })
      .catch(err => {
        this.setState({showLoading: false});
        Toast.show(`Error : ${err}`, Toast.SHORT);
      });
  };

  render() {
    return (
      <Container>
        <HeaderLayout
          navigation={this.props.navigation}
          title="Ubah Katasandi"
        />

        <Spinner
          visible={this.state.showLoading}
          color={styles.statusbarAccent.backgroundColor}
          size="large"
        />

        <Content>
          <Form>
            <Item stackedLabel>
              <Label>Password Lama</Label>
              <Input
                secureTextEntry={true}
                onChangeText={val => this.setState({pass_old: val})}
              />
            </Item>
            <Item stackedLabel>
              <Label>Password Baru</Label>
              <Input
                secureTextEntry={true}
                onChangeText={val => this.setState({pass_new: val})}
              />
            </Item>
            <Item stackedLabel last>
              <Label>Verifikasi Password</Label>
              <Input
                secureTextEntry={true}
                onChangeText={val => this.setState({verify: val})}
              />
            </Item>
          </Form>
          <Card>
            <CardItem header>
              <Text>Aturan Password :</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  1. Panjang karakter minimal 8 Karakter, maksimal 16 karakter
                </Text>
                <Text>2. Tidak mengandung unsur nip</Text>
                <Text>
                  3. Harus ada/mengandung minimal 1 huruf kecil dan 1 huruf
                  besar
                </Text>
                <Text>4. Harus ada/mengandung angka</Text>
                <Text>
                  5. Harus mengandung spesial karakter antara lain : & . # $ ! *
                  @
                </Text>
              </Body>
            </CardItem>
          </Card>
        </Content>

        <Footer>
          <FooterTab>
            <Button
              full
              style={{backgroundColor: '#0d8a43'}}
              onPress={this.submitForm}>
              <Text style={{color: '#fff', fontWeight: 'bold', fontSize: 15}}>
                Submit
              </Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

function mapStateToProps() {
  return {
    user: UserStore.getUserData(),
  };
}

export default connect(mapStateToProps)(ChangePassword);
