import * as remx from 'remx'

const initialState = {
  user: {},
}

const state = remx.state(initialState)

const getters = remx.getters({
  getUserData() {
    return state.user
  }

})

const setters = remx.setters({
  setUserData(payload) {
    state.user = payload
  }

})

export const store = {
  ...getters,
  ...setters
}
