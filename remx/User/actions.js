// @flow

import { store } from './store';
import { store as ServiceStore } from '../../remx/Service/store';
import Toast from 'react-native-simple-toast';
import DeviceInfo from 'react-native-device-info';
import { REST_URL, HEADERS_CONFIG } from 'larisa/AppConfig';
import AsyncStorage from "@react-native-community/async-storage";

export async function getUserDetail(nip?: String) {
  const uri = `${REST_URL}/rest/?method=DataPegawai&format=json&nip=${nip}`;
  fetch(uri, HEADERS_CONFIG)
    .then(response => response.json())
    .then(res => {
      if (typeof res.data[0] == 'object') {
        store.setUserData(res.data[0]);
      } else {
        store.setUserData({});
      }
    })
    .catch(err => {
      Toast.show(`Error getUserDetail:\n ${err}`, Toast.SHORT);
    });
}

export function checkSession() {
  let user = store.getUserData()
  if (!user.nip) return false

  let uri = `${REST_URL}/ImeiCheck.php?nip=${user.nip}&imei=${DeviceInfo.getUniqueId()}`;

  fetch(uri, HEADERS_CONFIG)
    .then(response => response.json())
    .then(res => {
      if (res.status != 'true') {
        AsyncStorage.clear().then(() => {
          this.props.navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });

          Toast.show(res.response, Toast.LONG)
          ServiceStore.setTodoListData([]);
          ServiceStore.setMyRequestData([]);

        });

      }
    }).catch(err => {
      Toast.show(`Error checkSession: ${err}`)
    })
};
