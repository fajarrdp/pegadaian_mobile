/* @flow */

import {store} from './store';
import Toast from 'react-native-simple-toast';
import {REST_URL, HEADERS_CONFIG} from 'larisa/AppConfig';

export async function getAbsensiData(nip?: String) {
  let m = store.getMonth();
  let tgl = store.getYear() + m;

  let uri = `${REST_URL}/rest/?method=kehadiran&format=json&nip=${nip}&periode=${tgl}`;
  fetch(uri, HEADERS_CONFIG)
    .then(response => response.json())
    .then(res => {
      store.setLoading(false);
      if (Array.isArray(res.data) && res.data.length > 0) {
        store.setData(res.data);
      } else {
        Toast.show(`Data absensi tidak ditemukan`, Toast.LONG);
      }
    })
    .catch(err => {
      store.setLoading(false);
      Toast.show(`Error getAbsensiData:\n ${err}`, Toast.SHORT);
    });
}

export async function setMonth(m?: Number) {
  store.setMonth(m.toString().length == 1 ? `0${m}` : m);
}

export async function setYear(y?: Number) {
  store.setYear(y);
}
